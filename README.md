# Pour installer l'application

1. Créer un clone du lien SSH sur GitLab

Puis, dans le terminal, taper les commandes ci-dessous :

2. npm ci
3. npm install typescript --save-dev
4. npm i --save-dev @types/node
5. npm i prompt-sync
6. npm i @types/prompt-sync

# Pour utiliser les scripts de simplonDelivery2

1. Dans la racine du projet, créer un dossier dist, puis un sous-dossier js. C'est là que seront transpilés les scripts.

2. Dans le terminal, lancer la commande : 

    npm start

Cette commande transpile tous les fichier.ts en fichier.js, et lance directement le script inscrit dans package.json. 
Une fois la commande lancée, tapez les réponses directement dans le terminal, pas besoin de phrases, seule la réponse est nécessaire.

# Pour modifier cette commande (si on veut voir un autre script)

1. Modifier dans package.json la ligne suivante et changer le chemin ou le nom du fichier selon les besoins :

        "start": "npx tsc && node dist/js/app.js"

# Exemple :
Voilà ce qui devrait apparaître dnas le terminal après la commande npm start : 

What are the dimensions of your package? (in cm) [number]
Height : 50
Width : 21
Length : 13
The size of your package is : 84cm.
How much does your package weigh? (in kg) [number]
Weight : 10
What is the delivery's destination? [name]
Country's name : Japan
Destination confirmed : japan
For a delivery in japan, it will cost you 3000¥.
Including the customs' fees, the delivery will cost 3000¥.
Do wish to convert this amount in another currency? [y/n] yes
Select a currency (EUR, JPY, CAD):
Current currency : JPY
Wanted currency : EUR
3000¥ vaut 23.1 €.