// Importation de Prompt à la place de ReadLine
import PromptSync from 'prompt-sync';

// le module prompt-sync est une fonction qui appelle d'autre fonctions, il faut donc l'appeler ()
// sigint: true (signal interrupt) permet à l'utilisateur de quitter le terminal, comme control + C
const prompt = PromptSync({sigint: true});

// On prédéfinit cost en tant que variable globale, pour être utilisée dans plusiuers fonctions
let cost: number;

// Questions pour les dimensions du paquet
console.log('What are the dimensions of your package? (in cm) [number]')
const height = prompt('Height : ')
const width = prompt('Width : ')
const length = prompt('Length : ')

// On convertit les string du prompt en Float puis affiche le résultat
const size = parseFloat(height) + parseFloat(width) + parseFloat(length)
console.log("The size of your package is : " + size + "cm.")

// Question pour le poids
console.log('How much does your package weigh? (in kg) [number]')
const answerWeight = prompt('Weight : ')
const weight = parseFloat(answerWeight)

// Question pour la destination
console.log('What is the delivery\'s destination? [name]')
let destination = prompt('Country\'s name : ')

// On valide les données pour la destination
function validateDestination() {
    //on change le résultat en minuscule, pour éviter la sensibilité à la casse
    destination = destination.toLowerCase();
    // Si le résultat indiqué est différent de ceux attendus, alors on renvoie une erreur
    if (destination === "france" || "canada" || "japan") {
        console.log(`Destination confirmed : ${destination}`)
    } else {
        throw new Error("The destination country must either be France, Japan or Canada. Only the country's name must be written.")
    }
}
validateDestination()

//-------------------------------------------------------------------------------
// DELIVERY COST
//-------------------------------------------------------------------------------

// L'utilisateur doit pouvoir entrer le poids du colis (en kg), les dimensions (longueur, largeur, hauteur en cm) et le pays de destination (France, Canada, Japon).
// Utilise les tarifs de livraison suivants basés sur le poids du colis :

// Jusqu'à 1 kg : 10 EUR, 15 CAD, 1000 JPY
// Entre 1 et 3 kg : 20 EUR, 30 CAD, 2000 JPY
// Plus de 3 kg : 30 EUR, 45 CAD, 3000 JPY


// Ajoute un supplément de 5 EUR, 7.5 CAD, 500 JPY pour les colis dont la somme des dimensions dépasse 150 cm.
// Retourne les frais de livraison dans la devise correspondant au pays de destination.

// Précisions sur les attendus de la fonction deliveryCost quand on passe la souris dessus
/**
 * 
 * @param weight number
 * @param size number
 * @param destination string "France", "Japan", "Canada"
 */

// Cette fonction attend un poids en kg et un nom de pays, les dimensions sont donénes par la fonction précédente
function deliveryCost(): string {

    // Si le poids du colis est entre 0 et 1 kg, alors..
    if (0 < weight && weight <= 1) {
        // en fonction de la destination...
        switch (destination) {
            case "france":
                // le coût diffère pour chaque pays
                cost = 10
                // et diffère en fonction de la taille
                if (size > 150) {
                    // ajoute et redéfinit la valeur du coût
                    cost += 5
                    return `For a delivery in ${destination}, it will cost you ${cost}€.`
                }
                return `For a delivery in ${destination}, it will cost you ${cost}€.`
            case "japan":
                cost = 1000
                if (size > 150) {
                    cost += 500
                    return `For a delivery in ${destination}, it will cost you ${cost}¥.`
                }
                return `For a delivery in ${destination}, it will cost you ${cost}¥.`
            case "canada":
                cost = 15
                if (size > 150) {
                    cost += 7.5
                    return `For a delivery in ${destination}, it will cost you ${cost}$.`
                }
                return `For a delivery in ${destination}, it will cost you ${cost}$.`
            default:
                throw new Error("Invalid destination. Please enter Japan, France, or Canada")
        }
    } else if (1 < weight && weight <= 3) {
        switch (destination) {
            case "france":
                cost = 20
                if (size > 150) {
                    cost += 5
                    return `For a delivery in ${destination}, it will cost you ${cost}€.`
                }
                return `For a delivery in ${destination}, it will cost you ${cost}€.`
            case "japan":
                cost = 2000
                if (size > 150) {
                    cost += 500
                    return `For a delivery in ${destination}, it will cost you ${cost}¥.`
                }
                return `For a delivery in ${destination}, it will cost you ${cost}¥.`
            case "canada":
                cost = 30
                if (size > 150) {
                    cost += 7.5
                    return `For a delivery in ${destination}, it will cost you ${cost}$.`
                }
                return `For a delivery in ${destination}, it will cost you ${cost}$.`
            default:
                throw new Error("Invalid destination. Please enter Japan, France, or Canada")
        }
    } else if (weight > 3) {
        switch (destination) {
            case "france":
                cost = 30
                if (size > 150) {
                    cost += 5
                    return `For a delivery in ${destination}, it will cost you ${cost}€.`
                }
                return `For a delivery in ${destination}, it will cost you ${cost}€.`
            case "japan":
                cost = 3000
                if (size > 150) {
                    cost += 500
                    return `For a delivery in ${destination}, it will cost you ${cost}¥.`
                }
                return `For a delivery in ${destination}, it will cost you ${cost}¥.`
            case "canada":
                cost = 45
                if (size > 150) {
                    cost += 7.5
                    return `For a delivery in ${destination}, it will cost you ${cost}$.`
                }
                return `For a delivery in ${destination}, it will cost you ${cost}$.`
            default:
                throw new Error("Invalid destination. Please enter Japan, France, or Canada")
        }
    } else {
        throw new Error("Invalid weight.")
    }
}

//-------------------------------------------------------------------------------
// CUSTOM FEE
//-------------------------------------------------------------------------------

// L'utilisateur doit pouvoir calculer les frais de douane basés sur la valeur déclarée du colis et le pays de destination (l'origine des colis sera toujours la France).

// Pour le Canada, 15 % de taxes pour les envois de plus de 20 CAD
// Pour le Japon, 10 % de taxes pour les envois de plus de 5000 JPY (qui n'arrivera jamais dans ce cas là)
// Pas de taxes supplémentaires pour la France.


// Retourne les frais de douane dans la devise correspondant au pays de destination.


function customFee() {
    switch (destination) {
        case "france":
            return `Including the customs' fees, the delivery will cost ${cost}€.`
        case "japan":
            if (cost > 3499) {
                cost = cost * 1.1
                return `Including the customs' fees, the delivery will cost ${cost}¥.`
            }
            return `Including the customs' fees, the delivery will cost ${cost}¥.`
        case "canada":
            if (cost > 20) {
                cost = cost * 1.5
                return `Including the customs' fees, the delivery will cost ${cost}$.`
            }
            return `Including the customs' fees, the delivery will cost ${cost}$.`
        default:
            throw new Error("Invalid destination country. Please enter, France, Japan, or Canada.")
    }
}

//console.log(customFee("Canada"))

console.log(deliveryCost())
console.log(customFee())

//-------------------------------------------------------------------------------
//  CONVERT
//-------------------------------------------------------------------------------

// L'utilisateur doit pouvoir entrer le montant à convertir, la devise de départ et la devise de destination.
// Utilise un tableau de taux de conversion fixes pour les devises suivantes :

// 1 EUR = 1.5 CAD = 130 JPY
// 1 CAD = 0.67 EUR = 87 JPY
// 1 JPY = 0.0077 EUR = 0.0115 CAD

// currency = euro (EUR), dollar canadien (CAD), yen, (JPY)
// chosen currency = euro (EUR), dollar canadien (CAD), yen, (JPY)

// Retourne le montant converti dans la devise de destination.

// On donne des valeurs globales
let currency: string;
let chosenCurrency: string;

/**
 * Convert an amount in a currency to another currency
 * @param cost number
 * @param currency string "EUR", "JPY", "CAD"
 * @param chosenCurrency string "EUR", "JPY", "CAD"
 * @returns a string with the result
 */

function convert(): string {

    switch (currency) {
        case "EUR":
            currency = "€"
            if (chosenCurrency === "CAD") {
                chosenCurrency = "$"
                let result: number = cost * 1.5
                return (`${cost}${currency} vaut ${result} ${chosenCurrency}.`)
            } else if (chosenCurrency === "JPY") {
                chosenCurrency = "¥"
                let result: number = cost * 130
                return (`${cost}${currency} vaut ${result} ${chosenCurrency}.`)
            } else {
                throw new Error("Invalid chosen currency")
            }

        case "JPY":
            currency = "¥"
            if (chosenCurrency === "CAD") {
                chosenCurrency = "$"
                let result: number = cost * 0.0115
                return (`${cost}${currency} vaut ${result} ${chosenCurrency}.`)
            } else if (chosenCurrency === "EUR") {
                chosenCurrency = "€"
                let result: number = cost * 0.0077
                return (`${cost}${currency} vaut ${result} ${chosenCurrency}.`)
            } else {
                throw new Error("Invalid chosen currency")
            }

        case "CAD":
            currency = "$"
            if (chosenCurrency === "EUR") {
                chosenCurrency = "€"
                let result: number = cost * 0.67
                return (`${cost}${currency} vaut ${result} ${chosenCurrency}.`)
            } else if (chosenCurrency === "JPY") {
                chosenCurrency = "¥"
                let result: number = cost * 87
                return (`${cost}${currency} vaut ${result} ${chosenCurrency}.`)
            } else {
                throw new Error("Invalid chosen currency")
            }

        default:
            throw new Error(`Invalid currency : ${currency}. Please enter EUR, JPY, or CAD.`);
    }
}

const questionCurrency = prompt("Do wish to convert this amount in another currency? [y/n] ") 

function askCurrency() {
    // On vérifie la réponse et lance la fonction convert() si voulu
    if (questionCurrency.toLowerCase() === "y" || questionCurrency.toLowerCase() === "yes") {
        console.log("Select a currency (EUR, JPY, CAD):")
        currency = prompt('Current currency : ')
        chosenCurrency = prompt('Wanted currency : ')
        console.log(convert())
    }
}

console.log(askCurrency())




